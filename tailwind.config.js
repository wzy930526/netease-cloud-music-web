/*
 * @Author: wangzy
 * @Date: 2022-06-23 14:07:55
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-23 16:37:08
 * @Description:
 */
/** @type {import('tailwindcss').Config} */
const getStyleMapping = (max, min, type) => {
  if (!max) {
    console.log('请传入max值')
    return
  }
  const style = {}
  let maxArray = [...Array(max + 1).keys()]
  if (min) {
    maxArray = maxArray.filter((item) => item >= min)
  }
  maxArray.forEach((item) => {
    if (!type) {
      style[item] = `${item}px`
    } else if (type === 'even') {
      item % 2 === 0 && (style[item] = `${item}px`)
    } else if (type === 'odd') {
      item % 2 !== 0 && (style[item] = `${item}px`)
    } else {
      console.log('你传的type到底是个啥')
    }
  })
  return style
}
module.exports = {
  darkMode: 'media', // or 'media' or 'class'
  content: [
    // Example content paths...
    './public/**/*.html',
    './src/**/*.{js,jsx,ts,tsx,vue}'
  ],
  theme: {
    extend: {},
    textColor: {
      primary: '#777777',
      secondary: '#D4917F',
      danger: '#e3342f',
      timeNam: '#cccccc',
      fff: '#fff',
      black: '#000000',
      colorb8: '#b8b8b8'
    },
    colors: {
      main: '#C20C0C',
      main2: '#C10D0C',
      main3: '#9B0909',
      black: '#000000'
    },
    padding: {
      ...getStyleMapping(600, 0)
    },
    margin: {
      center: '0 auto',
      auto: 'auto',
      ...getStyleMapping(200, 0)
    },
    borderRadius: {
      ...getStyleMapping(100, 1),
      full: '100%'
    },
    minWidth: {
      ...getStyleMapping(1000, 20)
    },
    minHeight: {
      ...getStyleMapping(1000, 20)
    },
    backgroundColor: (theme) => {
      return {
        ...theme('colors')
      }
    },
    extend: {
      lineHeight: {
        ...getStyleMapping(200, 0)
      },
      width: {
        ...getStyleMapping(1000, 0)
      },
      height: {
        ...getStyleMapping(1000, 0)
      },
      opacity: {
        85: '0.85'
      },
      inset: {
        ...getStyleMapping(400, 0)
      }
    },
    borderColor: (theme) => ({
      ...theme('colors'),
      E5: '#E5E5E5'
    })
  },
  plugins: []
}
