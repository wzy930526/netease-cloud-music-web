/*
 * @Author: wangzy
 * @Date: 2022-06-15 14:36:23
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-15 14:36:24
 * @Description:
 */
declare module '*.vue' {
  import { defineComponent } from 'vue'
  const Component: ReturnType<typeof defineComponent>
  export default Component
}
