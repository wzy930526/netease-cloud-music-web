import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import styleImport, {
  AndDesignVueResolve,
  VantResolve,
  ElementPlusResolve,
  NutuiResolve,
  AntdResolve,
} from 'vite-plugin-style-import'

import Components from 'unplugin-vue-components/vite'
import {
  AntDesignVueResolver,
  ElementPlusResolver,
  VantResolver,
} from 'unplugin-vue-components/resolvers'

import * as path from 'path'

export default defineConfig(({ command, mode }) => {
  const env = loadEnv(mode, __dirname)
  return {
    build: {
      outDir: 'dist',
      target: 'es2015',
      brotliSize: false,
      chunkSizeWarningLimit: 2000,
      terserOptions: {
        //打包后移除console和注释
        compress: {
          drop_console: true,
          drop_debugger: true,
          keep_infinity: true,
        },
      },
      minify: 'terser',
    },
    plugins: [
      vue(),
      styleImport({
        resolves: [
          AndDesignVueResolve(),
          VantResolve(),
          ElementPlusResolve(),
          NutuiResolve(),
          AntdResolve(),
        ],
        libs: [
          {
            libraryName: 'element-plus',
            esModule: true,
            resolveStyle: (name) => {
              return `element-plus/theme-chalk/${name}.css` // 按需引入样式
            },
          },
          {
            libraryName: 'ant-design-vue',
            esModule: true,
            resolveStyle: (name) => {
              return `ant-design-vue/es/${name}/style/index`
            },
          },
          {
            libraryName: 'vant',
            esModule: true,
            resolveStyle: (name) => {
              return `vant/lib/${name}/style/index`
            },
          },
        ],
      }),
      Components({
        resolvers: [
          AntDesignVueResolver(),
          ElementPlusResolver(),
          VantResolver(),
        ],
      }),
    ],
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
        'v': path.resolve(__dirname,'src/view',)
      },
    },
    publicDir: 'public',
    server: {
      open: true, //服务启动时自动在浏览器中打开应用
      host: 'localhost',
      port: 4000,
      proxy: {
        '/api': {
          target: env.VITE_APP_BASE_API,
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, ''),
        },
      },
    },
  }
})
