import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '@/view/home/index.vue'
import User from '@/view/me/index.vue'
import Song from '@/view/song/song.vue'
import Login from '@/view/login/index.vue'
export default createRouter({
  // 指定路由模式
  history: createWebHashHistory(),
  // 路由地址
  routes: [
    {
      path: '/',
      name: 'cover',
      component: () => import('@/view/cover/index.vue'),
      meta: {
        title: 'cover',
      },
    },
    {
      path: '/home/:id',
      name: 'home',
      component: () => import('@/view/home/index.vue'),
      meta: {
        title: 'home',
      },
    },

    {
      path: '/user',
      component: User,
      meta: {
        title: '春风十里user',
      },
    },
    {
      path: '/song',
      component: Song,
      meta: {
        title: '春风十里song',
      },
    },
  ],
})
