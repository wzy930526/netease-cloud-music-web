import axios from '@/http/request'

export const search_song = (data: object): Promise<any> => {
  return axios({
    url: `/search?keywords=${data.keywords}`,
    method: 'get',
    data,
  })
}
