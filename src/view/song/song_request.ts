import axios from '@/http/request'

export const get_song = (data: object): Promise<any> => {
  return axios({
    url: '/artist/top/song',
    method: 'get',
    data,
  })
}
