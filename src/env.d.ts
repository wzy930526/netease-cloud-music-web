/*
 * @Author: wangzy
 * @Date: 2021-12-29 10:49:48
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-15 14:38:37
 * @Description:
 */

import { DefineComponent } from 'vue'

interface ImportMetaEnv {
  NODE_ENV: string
  ENV: string
  VITE_APP_BASE_API: string
}
