import { createApp } from 'vue'
import App from '@/App.vue'
// 导入router和store
import router from '@/router/index'
import store from '@/store/index'
import VueWechatTitle from 'vue-wechat-title' //动态修改title
import 'vant/lib/index.css';
import '@/assets/css/index.css'
import '@/assets/css/reset.css'
import 'animate.css'
const app = createApp(App as any)

app.use(router).use(store).use(VueWechatTitle).mount('#app')
