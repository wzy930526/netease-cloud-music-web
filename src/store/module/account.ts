export const account = {
  namespaced: true, // 命名空间
  state() {
    return {
      songsList:[]
    }
  },
  getters: {

  },
  mutations: {
    // increment(state, payload) {
    //   state.homeCounter += payload
    // },
    changeSongsList(state, payload) {
      state.songsList = payload
    }
  },
  actions: {
    // incrementAction({ commit, state, rootState }, payload) {
    //   commit('increment', payload, { root: true }) // 在模块里调用state根的mutations里面的方法
    // },
  },
}
