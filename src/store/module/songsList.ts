export const songsList = {
  namespaced: true, // 命名空间
  state() {
    return {
      homeCounter: 100,
      age: 2222,
    }
  },
  getters: {
    // 命名空间里面，getters,新增了2个参数 rooteState,rootGetters
    doubleHomeCounter(state, getters, rooteState, rootGetters) {
      console.log('getters', getters)
      console.log('rooteState', rooteState)
      console.log('rootGetters', rootGetters)
      return state.homeCounter * 2
    },
  },
  mutations: {
    increment(state, payload) {
      state.homeCounter += payload
    },
  },
  actions: {
    incrementAction({ commit, state, rootState }, payload) {
      // commit('increment', payload)
      commit('increment', payload, { root: true }) // 在模块里调用state根的mutations里面的方法
    },
  },
}
