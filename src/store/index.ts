import { createStore } from 'vuex'
import { account } from './module/account'
import {songsList} from './module/songsList'

export default createStore({
  state: {
    name: 'Bryant',
    count: 0,
    clientWidth: 0,
    rootCounter: 24,
    increment: 0,
  },
  getters: {
    // 命名空间里面，getters,新增了2个参数 rooteState,rootGetters
    rootRootCounter(state, getters, rooteState, rootGetters) {
      return state.rootCounter * 2
    },
  },
  mutations: {
    increment(state, payload) {
      state.increment += payload
    },

    clientWidthMutations(state, clientWidth) {
      state.clientWidth = clientWidth
    },
  },
  modules: {
    account,
    songsList
  },
})
