/*
 * @Author: wangzy
 * @Date: 2022-06-15 16:40:27
 * @LastEditors: wangzy
 * @LastEditTime: 2022-06-23 14:36:43
 * @Description:
 */
function remSize() {
  // 获取设备的宽度
  let deviceWidth = document.documentElement.clientWidth || window.innerWidth

  if (deviceWidth >= 750) {
    deviceWidth = 750
  }
  if (deviceWidth > 375) {
    deviceWidth = 375
  }

  // 750px ---> 1rem === 100px, 375px--->1rem=50px
  document.documentElement.style.fontSize = deviceWidth / 7.5 + 'px'
  // 字体大小
  document.querySelector('body').style.fontSize = 0.3 + 'rem'
}
remSize()
// 当窗口发生变化就会调用
window.addEventListener('resize', () => {
  remSize()
})
